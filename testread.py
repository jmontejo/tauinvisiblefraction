import pandas as pd

name = 'test.h5'
name = 'tau_dataset.h5'

df_tau = pd.read_hdf(name, key='tau')
df_cls = pd.read_hdf(name, key='tau_cls')
df_trk = pd.read_hdf(name, key='tau_trk')
df_pi0 = pd.read_hdf(name, key='tau_pi0')
print(df_tau)
print(df_cls)
print(df_trk)
print(df_pi0)
