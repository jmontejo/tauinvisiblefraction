import uproot
import awkward as ak
import numpy as np
import pandas as pd

test = False
doMinimalVars = False

if test:
    infilepath = "user.martindl.InvisMomentum.801002.Py8EG_A14NNPDF23LO_Gammatautau_MassWeight_v1_output.root/test.root"
else:
    infilepath = "user.martindl.InvisMomentum.801002.Py8EG_A14NNPDF23LO_Gammatautau_MassWeight_v1_output.root/user.martindl.33595671._000001.output.root"

variables1D = [
 "ChPiEMEOverCaloEME",
 "ClustersMeanCenterLambda",
 "ClustersMeanEMProbability",
 "ClustersMeanFirstEngDens",
 "ClustersMeanPresamplerFrac",
 "ClustersMeanSecondLambda",
 "EMFRACTIONATEMSCALE_MOVEE3",
 "EMFracFixed",
 "EMPOverTrkSysP",
# "IsTruthMatched",
 "PFOEngRelDiff",
 "PSSFraction",
 "PanTau_DecayMode",
 "PanTau_DecayModeExtended",
 "PanTau_DecayModeProto",
 "PanTau_isPanTauCandidate",
 "RNNJetScore",
 "RNNJetScoreSigTrans",
 "SumPtTrkFrac",
 "TAU_SEEDTRK_SECMAXSTRIPETOVERPT",
 "TVz",
 "centFrac",
 "dRmax",
 "etHotShotDR1",
 "etHotShotDR1OverPtLeadTrk",
 "etHotShotWin",
 "etHotShotWinOverPtLeadTrk",
 "etOverPtLeadTrk",
 "eta",
 "etaCharged",
 "etaDetectorAxis",
 "etaIntermediateAxis",
 "etaPanTauCellBased",
 "etaPi0",
 "etaTauEnergyScale",
 "hadLeakEt",
 "hadLeakFracFixed",
 "innerTrkAvgDist",
 "isTauFlags",
# "m",
 "mCharged",
 "mDetectorAxis",
 "mEflowApprox",
 "mIntermediateAxis",
 "mPanTauCellBased",
 "mPi0",
# "mTauEnergyScale",
 "massTrkSys",
 "mcEventNumber",
 "mu",
 "nModifiedIsolationTracks",
 "nPi0PFO",
 "nTracks",
 "nTracksIsolation",
 "nVtxPU",
 "phi",
 "phiCharged",
 "phiDetectorAxis",
 "phiIntermediateAxis",
 "phiPanTauCellBased",
 "phiPi0",
 "phiTauEnergyScale",
 "pt",
 "ptCharged",
 "ptCombined",
 "ptDetectorAxis",
 "ptFinalCalib",
 "ptIntermediateAxis",
 "ptPanTauCellBased",
 "ptPi0",
 "ptRatioEflowApprox",
 "ptTauEnergyScale",
 "secMaxStripEt",
 "sumEMCellEtOverLeadTrkPt",
 "trFlightPathSig",
 "trk_etaJetSeed",
 "trk_nTracksFiltered",
 "trk_nTracksTotal",
 "trk_phiJetSeed",
 "trk_ptJetSeed",
 "truthDecayMode",
 "truthEtaInvis",
 "truthEtaVis",
 "truthEtaVisCharged",
 "truthEtaVisDressed",
 "truthEtaVisNeutral",
 "truthMInvis",
 "truthMVis",
 "truthMVisCharged",
 "truthMVisDressed",
 "truthMVisNeutral",
# "truthParticleOrigin",
# "truthParticleType",
 "truthPhiInvis",
 "truthPhiVis",
 "truthPhiVisCharged",
 "truthPhiVisDressed",
 "truthPhiVisNeutral",
 "truthProng",
 "truthPtInvis",
 "truthPtVis",
 "truthPtVisCharged",
 "truthPtVisDressed",
 "truthPtVisNeutral",
 "cls_nClustersTotal",
]

variables2D = [
 "pi0_eta",
# "pi0_m",
 "pi0_phi",
 "pi0_pt",
 "trk_charge",
 "trk_chargedScoreRNN",
 "trk_chiSquared",
 "trk_conversionScoreRNN",
 "trk_d0SigTJVA",
 "trk_d0TJVA",
 "trk_dEta",
 "trk_dPhi",
 "trk_eta",
 "trk_fakeScoreRNN",
 "trk_isCharged",
 "trk_isolationScoreRNN",
 "trk_nDoF",
 "trk_nInnermostPixelHits",
 "trk_nPixelHits",
 "trk_nSCTHits",
 "trk_phi",
 "trk_pt",
 "trk_z0sinthetaSigTJVA",
 "trk_z0sinthetaTJVA",
 "cls_CENTER_LAMBDA",
 "cls_SECOND_LAMBDA",
 "cls_SECOND_R",
 "cls_dEta",
 "cls_dPhi",
 "cls_e",
 "cls_et",
 "cls_eta",
 "cls_phi",
]

if doMinimalVars:
    variables1D = ["pt","eta","phi","m"]
    variables2D = ["trk_d0SigTJVA","cls_et","pi0_pt", "trk_isCharged"]

base = "TauJetsAuxDyn."
variables1D = [base+v for v in variables1D]
variables2D = [base+v for v in variables2D]

tau_inputs = {'cls':6, 'trk': 10 ,'pi0':3}

with uproot.open(infilepath) as infile:
    tree = infile["CollectionTree"]
    store = pd.HDFStore('test.h5' if test else 'tau_dataset.h5')
    events = tree.arrays(variables1D+variables2D, library="ak")
    charged_convert = base+"trk_isCharged"
    if charged_convert in variables2D:
        events[charged_convert] = ak.values_astype(events[charged_convert], np.int32)

    df1d = ak.to_dataframe(events[variables1D]).reset_index( drop=True)
    store.put('tau', df1d, format='table')
    print(df1d.head())
    print(df1d.shape)

    print(events[variables2D])
    print(ak.to_dataframe(events[variables2D]).reset_index(0, drop=True))

    for var, varmax in tau_inputs.items():
        vars2d = [x for x in variables2D if x.startswith(base+var)]
        ak_pad = ak.pad_none(events[vars2d], varmax, axis=-1, clip=True)
        df2d = ak.to_dataframe(ak_pad).reset_index(0, drop=True)
        store.put(f'tau_{var}', df2d, format='table')
        print(df2d.head())
        print(df2d.shape)

    #for i, events in enumerate( events.iterate(variables1D+variables2D, library="ak", step_size="50 kB") ):
    #    df1d = ak.to_dataframe(events[variables1D]).reset_index( drop=True)
    #    df2d = ak.to_dataframe(events[variables2D]).reset_index(0, drop=True)
    #    if i==0:
    #        store.put('tau', df1d, format='table')
    #        store.put('tau_inputs', df2d, format='table')
    #    else:
    #        store.append('tau', df1d)
    #        store.append('tau_inputs', df2d)
    store.close()

        #print(ak.to_dataframe(events[:2]))
        #print(ak.to_dataframe(events[:2]).reset_index())
        #print(ak.to_dataframe(events[:2]).reset_index(0, drop=True).describe())
        #print(ak.concatenate(events))
        #flat = ak.flatten(events, axis=2)
        #events2d = ak.fill_none(ak.pad_none(ak.flatten(events[variables2D], axis=1), 10, clip=True), np.float32(0))
        #print(events)
        #print(events1d)
        #print(events2d)

#with uproot.open(infilepath) as infile:
#    events = infile["CollectionTree"]
#    invars_ak = events.arrays(variables2D, library="ak")
#    print('ak',invars_ak[variables2D].type)
#    print('ak',invars_ak["TauJetsAuxDyn.trk_d0SigTJVA"][0])
#    flat = ak.flatten(invars_ak["TauJetsAuxDyn.trk_d0SigTJVA"], axis=1)
#    print('ak',flat.type)
#    print('ak', ak.fill_none(ak.pad_none(flat, 10, clip=True),np.float32(0)).type)
