# Estimate the neutrino pT in a tau decay

### Files

`/eos/home-j/jmontejo/TauInvisible/tau_dataset.h5`

### Variables

- pt is the MVA pt we use in analyses, should be identical to ptFinalCalib
- all these ones are intermediate calibrations and can be ignored:
ptCombined, ptDetectorAxis, ptIntermediateAxis, ptPanTauCellBased, ptTauEnergyScale
- ptRatioEflowApprox is something used in tau ID, I think, ignore too
- ptCharged and ptPi0, I need to check, it's not standard variables, for performance studies maybe, but we never use these
- pi0_pt is the pt of reconstructed tau pi0s
- PanTau_DecayMode = 5-way decay mode classification from subtructure reco, now superseded by the NN decay mode classificiation
- PanTau_DecayModeProto = best guess before substructure reconstruction runs (PanTau has 3 BDTs to try and improve this initial guess, separating 1p0n/1p1n, 1p1n/1pXn, 3p0n/3pXn)
- PanTau_DecayModeExtended: never used this, I think it may go a bit beyond the 5-way classification
- mPanTauCellBased is the tau mass coming from substructure, possibly the only meaningful tau mass we have (others are set to 0), I assume it's what is plotted here: https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/PERF-2014-06/fig_10b.png
- trk_chargedScoreRNN = probability that a track is a "classifiedCharged" track, which is 1 of the 4 track categories of the tau track classifier (classifiedCharged = charged pion track from tau decay in the case of taus, for jets it has a different meaning, track originating from the highest-pt parton of the jet, it's a bit ill-defined)
- trFlightPathSig = flight path significance, only computed for >=2p from the production and decay vertices + covariance matrix: https://gitlab.cern.ch/atlas/athena/-/blob/main/Reconstruction/tauRecTools/src/TauVertexVariables.cxx#L126
- etOverPtLeadTrk = https://gitlab.cern.ch/atlas/athena/-/blob/main/Reconstruction/tauRecTools/Root/TauCommonCalcVars.cxx#L65
- cls_dPhi = I think it's dPhi(cluster,tau) with the tau axis from the substructure reconstruction
- mEflowApprox = coarse mass estimate computed from tracks and clusters, without substructure: https://gitlab.cern.ch/atlas/athena/-/blob/main/Reconstruction/tauRecTools/Root/TauSubstructureVariables.cxx#L177


what shouldn't be used for analysis:  
- PanTau_DecayXX, we now have the NN classification that is performing better
https://gitlab.cern.ch/atlas/athena/-/blob/main/Reconstruction/tauRecTools/Root/TauDecayModeNNClassifier.cxx#L30

the rest I guess can be used, but doesn't come with dedicated recommendations

if you find that in particular one is powerful, maybe it would be good to discuss its usage with TauCP regarding recommendations

trk_XXXScoreRNN is maybe delicate to use, it has been tried as input to a variant of DeepSet ID, and gave strange results, it seemed to improve jet rejection in some cases, but the WP efficiencies for true taus were all messed up and became process-dependent...

### Tracks

trk_isChargeda true if it is a tau track classified as track from the tau decay

so in principle, if you care about reconstructed tau decay products, it would be tracks with trk_isCharged=true, and the pi0s

all other tracks that are present are simply matched to the tau but not used?
we use all tracks (10 leading pt) in tau ID

in 1 place, we also use "isolation tracks"

and "conversion tracks" are used in the decay mode NN classifier

fake/pileup tracks are used nowhere

for 95% of analysis use cases, people only care about "classifiedCharged" tracks, those with trk_isCharged=true in MxAOD

but for algorithms like tau ID, it's good to make ML aware of as many "relevant" tracks as possible

in PHYS, we thin all tracks except the classifiedCharged

### Availability in DAOD_PHYS

All the tau variables
https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/DerivationFramework/DerivationFrameworkTau/python/TauJetsCPContent.py


and you get a couple of extra variables directly implanted in PHYS, don't ask why...
https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/DerivationFramework/DerivationFrameworkPhys/python/PHYS.py#L123
